export default interface Product {
  id?: number;
  name: string;
  tel: string;
  point: number;
  start_date: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
